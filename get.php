<?php
include "config.php";
$sql = mysqli_query($conn, "select * from counts order by id asc");
while($data = mysqli_fetch_array($sql)){
    ?>
    <div class="row">
        <div class="col-md-6">
            <center><img src="tf_img.png" class="shadow p-3 mb-5 bg-white rounded" width="30%"></center>
        </div>
        <div class="col-md-6">
            <h3><?= $data['nm_jalan'] ?></h3>
            <h5 class="text-center">Jumlah Kendaraan <a href="detail.php?id=<?= $data['kd_infra'] ?>" class="btn btn-sm btn-info">Detail</a> </h5>
            <h1 class="text-center" style="font-size: 6rem"><u><?= $data['count_infra'] ?></u></h1><br/>
        </div>
    </div>
<?php } ?>
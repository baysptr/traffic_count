#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

#define USE_SERIAL Serial
#define mySSID "UNDEFINED NETWORK"
#define myPASS "sembarangtii"
#define kd_infra "FSLKM-5c3fda68d9974";

ESP8266WiFiMulti WiFiMulti;

int num_ssid = 0;

void setup() {
  USE_SERIAL.begin(115200);

  USE_SERIAL.println();
  USE_SERIAL.println();
  USE_SERIAL.println();

  for (uint8_t t = 4; t > 0; t--) {
    USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
    USE_SERIAL.flush();
    delay(1000);
  }

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(mySSID, myPASS);
  while((WiFiMulti.run() != WL_CONNECTED)){
    USE_SERIAL.print(".");
    delay(1000);
  }
  USE_SERIAL.print("Your Connect on ");
  USE_SERIAL.println(mySSID);
}

void loop() {
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0) {
    Serial.println("no networks found");
  } else {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i) {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      delay(10);
      find_patterns(WiFi.SSID(i));
      sendNopol(WiFi.SSID(i));
    }
    sendReq(num_ssid);
  }
  delay(20000);
}

void sendReq(int num_ssids){
    String url = "http://80.211.184.148/traffic_count/iot.php?counts=";
    url += num_ssids;
    url += "&kd_infra=";
    url += kd_infra;
  
    HTTPClient http;
    http.begin(url); //HTTP
    USE_SERIAL.print("[HTTP] GET...\n");
    int httpCode = http.GET();
    if (httpCode > 0) {
      USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);
      if (httpCode == HTTP_CODE_OK) {
        String payload = http.getString();
        USE_SERIAL.println(payload);
        num_ssid = 0;
      }
    } else {
      USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }
    http.end();
}

void sendNopol(String nopol){
    String check = splitValue(nopol, 0);
  
    if(check == "FSLKM"){
      String url = "http://80.211.184.148/traffic_count/send_nopol.php?nopol=";
      url += splitValue(nopol, 1);
      url += "&kd_infra=";
      url += kd_infra;
    
      HTTPClient http;
      http.begin(url); //HTTP
      http.addHeader("Content-Type", "application/x-www-form-urlencoded");
      USE_SERIAL.println("[HTTP] GET... ");
      USE_SERIAL.println(url);
      int httpCode = http.GET();
      if (httpCode > 0) {
        USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);
        if (httpCode == HTTP_CODE_OK) {
          String payload = http.getString();
          USE_SERIAL.println(payload);
        }
      } else {
        USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }
      http.end();
    }
}

void find_patterns(String val){
  String pattern = splitValue(val, 0);
  if(pattern == "FSLKM"){
    num_ssid += 1;
  }
}

String splitValue(String data, int index){
  char separator = '-';
  int found = 0;
  int strIndex[]={0, -1};
  int maxIndex = data.length()-1;
  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i) == separator || i==maxIndex){
      found++;
      strIndex[0] = strIndex[1]+1;
      strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}

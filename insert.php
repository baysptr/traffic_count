<!doctype html>
<html lang="en" class="h-100">
<head>
    <title>Monitoring Traffic Light | WEB</title>
    <!-- Bootstrap core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="bower_components/bootstrap/dist/css/sticky-footer.css" rel="stylesheet">
</head>
<body class="d-flex flex-column h-100">
<main role="main" class="flex-shrink-0">
    <div class="container"><br/>
        <form method="post" action="do_insert.php">
            <div class="form-group">
                <label for="nama-jalan">Nama Jalan</label>
                <input type="text" class="form-control" name="nama_jalan" id="nama-jalan" aria-describedby="nama_jalan" placeholder="Tuliskan nama jalan" autofocus>
                <small id="nama_jalan" class="form-text text-muted">Masukan alamat jalan tempat infra di pasang</small>
            </div>
            <button type="submit" class="btn btn-primary">Save!</button>
        </form><br/>
        <table class="table table-striped table-bordered">
            <thead class="table-dark">
                <tr>
                    <td><strong>NAMA JALAN</strong></td>
                    <td><strong>KODE INFRA</strong></td>
                </tr>
            </thead>
            <tbody>
            <?php
                include "config.php";
                $sql = mysqli_query($conn, "select * from counts order by id asc");
                while($data = mysqli_fetch_array($sql)){
            ?>
                <tr>
                    <td><?= $data['nm_jalan'] ?></td>
                    <td><?= $data['kd_infra'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</main>

<footer class="footer mt-auto py-3">
    <div class="container">
        <span class="text-muted">&copy; Fasilkom, Universitas Narotama 2019</span>
    </div>
</footer>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</html>

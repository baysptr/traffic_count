<!doctype html>
<html lang="en" class="h-100">
<head>
    <title>Monitoring Traffic Light | WEB</title>
    <!-- Bootstrap core CSS -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="bower_components/bootstrap/dist/css/sticky-footer.css" rel="stylesheet">
</head>
<body class="d-flex flex-column h-100">
<main role="main" class="flex-shrink-0">
    <div class="container"><br/>
        <div id="det_data"></div><br/>
        <center><a href="index.php" class="btn btn-sm btn-info">Back</a></center><br/>
        <div id="det_nopol"></div>
    </div>
</main>

<footer class="footer mt-auto py-3">
    <div class="container">
        <span class="text-muted">&copy; Fasilkom, Universitas Narotama 2019</span>
    </div>
</footer>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $.ajaxSetup({ cache: false }); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
        setInterval(function() {
            $('#det_data').load('get_det.php?id=<?= $_GET["id"] ?>');
            $('#det_nopol').load('det_nopol.php?id=<?= $_GET["id"] ?>');
        }, 3000); // the "3000"
    });
</script>
</html>
